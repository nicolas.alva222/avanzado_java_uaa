package swing;

import javax.swing.*;

public class SumaDialogo {
    public static void main(String[] args) {
        String numero_uno = JOptionPane.showInputDialog("Introduzca el primer numero");
        String numero_dos = JOptionPane.showInputDialog("Introduzca el segundo numero");

        int result = 0;
        try {
            result = Integer.parseInt(numero_uno) + Integer.parseInt(numero_dos);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }

        JOptionPane.showMessageDialog(null,
                "Resultado de la suma; " + result,
                "Resultado",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
