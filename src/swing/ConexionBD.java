package swing;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConexionBD {
    private String driver = "org.postgresql.Driver";
    private String host = "jdbc:postgresql://localhost:5433/swing";
    private String user = "postgres";
    private String pass = "root";
    private Connection bd = null;

    public Connection conectarBD() throws SQLException {
        try {
            Class.forName(driver);
            bd = DriverManager.getConnection(host, user, pass);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bd;
    }

    public void cerrarBD() throws SQLException {
        if (bd != null) {
            bd.close();
        }
    }

    public boolean insertarRegistro(String nombre, String fabr, int cant) throws SQLException {
        String sql = "INSERT INTO public.producto(nombre, fabricante, cantidad) VALUES (?, ?, ?)";
        PreparedStatement st = bd.prepareStatement(sql);
        st.setString(1, nombre);
        st.setString(2, fabr);
        st.setInt(3, cant);
        int resultado = st.executeUpdate();
        st.close();
        if (resultado > 0) {
            return true;
        }
        return false;

    }

    public boolean eliminar(Long id) throws SQLException {
        String sql = "DELETE FROM public.producto WHERE cod_producto = ?";
        PreparedStatement st = bd.prepareStatement(sql);
        st.setLong(1, id);
        int cantidad = st.executeUpdate();
        st.close();
        if (cantidad == 0) {
            return false;
        }
        return true;
    }

    public boolean actualizar(Long id, String nombre, String fabr, int cant) throws SQLException {
        String sql = "UPDATE public.producto SET nombre=?, fabricante=?, cantidad=? WHERE cod_producto=?";
        PreparedStatement st = bd.prepareStatement(sql);
        st.setString(1, nombre);
        st.setString(2, fabr);
        st.setInt(3, cant);
        st.setLong(4, id);
        int cantidad = st.executeUpdate();
        st.close();
        if (cantidad == 0) {
            return false;
        }
        return true;
    }
    
    public ResultSet obtenerTodos() throws SQLException{
        String sql = "SELECT cod_producto, nombre, fabricante, cantidad FROM public.producto";
        PreparedStatement st = bd.prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        return rs;
    }
    public ResultSet obtenerDatosFiltrados(String nombre, String fabricante, Integer cantidad) throws SQLException{
        String sql = "SELECT cod_producto, nombre, fabricante, cantidad FROM public.producto where 1=1";
        if(nombre != null){
            sql = sql +" and nombre like ? ";
        }
        if(fabricante != null){
            sql = sql +" and fabricante like ? ";
        }
        if(cantidad != null){
            sql = sql +" and cantidad = ? ";
        }
        PreparedStatement st = bd.prepareStatement(sql);
        if(nombre != null && fabricante == null && cantidad == null)
            st.setString(1, nombre);
         if(nombre == null && fabricante != null && cantidad == null)
            st.setString(1, fabricante);
        if(nombre == null && fabricante == null && cantidad != null)
            st.setInt(1, cantidad);
        if(nombre != null && fabricante != null && cantidad == null){
            st.setString(1, nombre);
            st.setString(2, fabricante);
        }
        if(nombre != null && fabricante == null && cantidad != null){
            st.setString(1, nombre);
            st.setInt(2, cantidad);
        }
        if(nombre == null && fabricante != null && cantidad != null){
            st.setString(1, fabricante);
            st.setInt(2, cantidad);
        }
        if(nombre != null && fabricante != null && cantidad != null){
            st.setString(1, nombre);
            st.setString(2, fabricante);
            st.setInt(3, cantidad);
        }
        ResultSet resultado = st.executeQuery();
        return resultado;
    }

}
