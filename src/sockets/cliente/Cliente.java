package sockets.cliente;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cliente {

    private Socket socketCliente = null;
    private BufferedReader entrada = null;
    private DataInputStream entradaDatos = null;
    private DataOutputStream salidaDatos = null;
    private int numeroPuerto;
    private String ipMaquina;

    public int getNumeroPuerto() {
        return numeroPuerto;
    }

    public String getIpMaquina() {
        return ipMaquina;
    }

    public Cliente(int numeroPuerto, String ipMaquina) {
        this.numeroPuerto = numeroPuerto;
        this.ipMaquina = ipMaquina;
        this.conexion();
    }

    private void conexion() {
        try {
            socketCliente = new Socket(this.getIpMaquina(), this.getNumeroPuerto());

            Thread subProceso = new Thread(new Runnable() {
                @Override
                public void run() {
                    ingresarDatos();
                }
            });

            subProceso.start();

        } catch (ConnectException con) {
            Logger.getLogger(Cliente.class.getName()).log(Level.WARNING, "Por favor, verifique que el servidor este levantado en la maquina con IP: "  + this.getIpMaquina() + " y escuche el PUERTO: " + this.getNumeroPuerto(), con.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void ingresarDatos() {
        boolean repetirIteracion = true;
        boolean errorIngresarDatos = true;
        int numeroUno = 0;
        int numeroDos = 0;
        while (repetirIteracion) {

            try {
                entrada = new BufferedReader(new InputStreamReader(System.in));

                int opcion = 0;
                while (opcion != 5) {
                    System.out.println("------------------------------------");
                    System.out.println("Opciones disponibles");
                    System.out.println("------------------------------------");
                    System.out.printf("1- Suma\n" +
                            "2- Resta\n" +
                            "3- Multiplicacion\n" +
                            "4- Division\n" +
                            "5- Salir\n" +
                            "6- Salir y apagar el Servidor\n");
                    System.out.println("------------------------------------");
                    System.out.println("Seleccione una opcion:");

                    do {
                        try {
                            opcion = Integer.parseInt(entrada.readLine());
                            errorIngresarDatos = false;
                        } catch (NumberFormatException num) {
                            System.out.println("**Ingrese una opcion de 1 a 6**");
                            errorIngresarDatos = true;
                        }
                    } while (errorIngresarDatos);

                    if (opcion < 1 || opcion > 6) {
                        System.out.println("**Ingrese una opcion de 1 a 6**");
                        continue;
                    }

                    if (opcion == 5) {
                        repetirIteracion = false;
                        continue;
                    }

                    if (opcion >= 1 && opcion <= 4) {

                        do {
                            try {
                                System.out.println("Ingrese primer numero");
                                numeroUno = Integer.parseInt(entrada.readLine());
                                errorIngresarDatos = false;
                            } catch (NumberFormatException num) {
                                System.out.println("**Ingrese un numero valido**");
                                errorIngresarDatos = true;
                            }
                        } while (errorIngresarDatos);

                        do {
                            try {
                                System.out.println("Ingrese segundo numero");
                                numeroDos = Integer.parseInt(entrada.readLine());
                                errorIngresarDatos = false;
                            } catch (NumberFormatException num) {
                                System.out.println("**Ingrese un numero valido**");
                                errorIngresarDatos = true;
                            }
                        } while (errorIngresarDatos);

                        enviarDatos(opcion + ";" + numeroUno + ";" + numeroDos + "\n");
                    }

                    if (opcion == 6) {
                        enviarDatos(opcion + ";" + numeroUno + ";" + numeroDos + "\n");
                    }

                    if (escucharDatos(socketCliente)) {
                        repetirIteracion = false;
                        opcion = 5;
                    }
                    Thread.sleep(5000);
                }

            } catch (Exception ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);

            }

        }
        this.cerrarTodo();
    }

    private boolean escucharDatos(Socket socket) throws IOException {
        InputStream inputStream = null;
        String cadenaRecibida = null;
        inputStream = socket.getInputStream();
        entradaDatos = new DataInputStream(inputStream);
        cadenaRecibida = entradaDatos.readUTF();
        System.out.println(cadenaRecibida);

        if (cadenaRecibida.equals("Server socket apagado\n")) {
            return true;
        } else {
            return false;
        }

    }

    private void enviarDatos(String datos) {

        try {
            OutputStream outputStream = null;
            outputStream = socketCliente.getOutputStream();
            salidaDatos = new DataOutputStream(outputStream);
            salidaDatos.writeUTF(datos);
            salidaDatos.flush();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cerrarTodo() {
        try {

            if (salidaDatos != null) {
                salidaDatos.close();
            }

            if (entradaDatos != null) {
                entradaDatos.close();
            }

            if (socketCliente != null) {
                socketCliente.close();
                System.out.printf("***El Socket en el puerto:%d se bajo***\n", this.getNumeroPuerto());
            }

        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
