package sockets.cliente;

public class ClienteSocket {
    
    private static Cliente cliente;
	private static final int PUERTO_SERVER = 8082;
	private static final String HOST_SERVER = "localhost";

    public static void main(String[] args) {
       cliente = new Cliente(PUERTO_SERVER, HOST_SERVER);
    }
    
}
