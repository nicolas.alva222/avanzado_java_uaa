package sockets.servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {
    private int puertoServidor;
	private Socket miServicio = null;
	private ServerSocket socketServicio = null;
	private DataOutputStream salidaDatos = null;;
	private DataInputStream entradaDatos = null;;
	private boolean repetirIteracion = true;
	private boolean terminarServidor = false;

	public int getPuertoServidor() {
		return puertoServidor;
	}

	public Servidor(int puertoServidor) {
		this.puertoServidor = puertoServidor;
		this.conexion();
	}

	private void conexion() {
		try {
			socketServicio = new ServerSocket(this.getPuertoServidor());
			miServicio = socketServicio.accept();
			System.out.printf("***El Servidor esta escuchando el puerto:%d***\n", this.getPuertoServidor());

			Thread subProceso = new Thread(new Runnable() {
				@Override
				public void run() {
					recibirDatos();
				}
			});

			subProceso.start();

		} catch (BindException e) {
			Logger.getLogger(Servidor.class.getName()).log(Level.WARNING, "El puerto esta en uso - Puerto " + this.getPuertoServidor(), e.getMessage());
		} catch (Exception ex) {
			Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private synchronized void recibirDatos() {
		while (repetirIteracion) {
			try {
				InputStream inputStream = null;
				String lineaRecibida = null;
				String parametroRecibido[];

				inputStream = miServicio.getInputStream();
				entradaDatos = new DataInputStream(inputStream);

				lineaRecibida = entradaDatos.readUTF();
				parametroRecibido = lineaRecibida.split(";");
				this.calcularResultado(parametroRecibido);

			} catch (IOException ex) {
				try {
					if (miServicio != null) {
						miServicio.close();
					}

					if (entradaDatos != null) {
						entradaDatos.close();
					}

					miServicio = socketServicio.accept();
				} catch (IOException e) {
					e.printStackTrace();
				}
				;
			}
		}
	}

	private void calcularResultado(String parametro[]) {
		float numeroUno = 0, numeroDos = 0, resultado = 0;
		String escribir = null, operacion = null;

		if (parametro.length == 3) {
			if (Integer.valueOf(parametro[0]) == 6) {
				escribir = "Server socket apagado\n";
				this.terminarServidor = true;
			} else {
				numeroUno = Float.valueOf(parametro[1]);
				numeroDos = Float.valueOf(parametro[2]);

				switch (Integer.valueOf(parametro[0])) {
				case 1:
					operacion = "Suma";
					resultado = numeroUno + numeroDos;
					break;
				case 2:
					operacion = "Resta";
					resultado = numeroUno - numeroDos;
					break;
				case 3:
					operacion = "Multiplicacion";
					resultado = numeroUno * numeroDos;
					break;
				case 4:
					operacion = "Division";
					resultado = numeroUno / numeroDos;
					break;
				}
				escribir = "Respuesta del Servidor: La "
						+ operacion + " de " + numeroUno + " y "
						+ numeroDos + " da como resultado: "
						+ resultado + "\n";
			}

		} else {
			escribir = "Respuesta del Servidor: Cantidad de parametros recibidos incorrectos\n";
		}

		this.enviarDatos(escribir);

	}

	private void enviarDatos(String datos) {
		try {
			OutputStream outputStream = null;
			outputStream = miServicio.getOutputStream();
			salidaDatos = new DataOutputStream(outputStream);
			salidaDatos.writeUTF(datos);
			salidaDatos.flush();

			if (terminarServidor) {
				this.cerrarTodo();
				this.repetirIteracion = false;
			}

		} catch (IOException ex) {
			Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	private void cerrarTodo() {
		try {

			if (salidaDatos != null) {
				salidaDatos.close();
			}

			if (entradaDatos != null) {
				entradaDatos.close();
			}

			if (socketServicio != null) {
				socketServicio.close();
				System.out.printf("***El Servidor en el puerto:%d esta abajo***\n", this.getPuertoServidor());
			}

			if (miServicio != null) {
				miServicio.close();
			}

		} catch (IOException ex) {
			Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
