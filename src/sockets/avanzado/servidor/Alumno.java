package sockets.avanzado.servidor;

import java.util.Objects;

public class Alumno {
    String cedula;
    String nombreCompleto;
    String carrera;
    char semestre;
    String materia;

    public Alumno() {
    }

    public Alumno(String cedula, String nombreCompleto, String carrera, char semestre, String materia) {
        this.cedula = cedula;
        this.nombreCompleto = nombreCompleto;
        this.carrera = carrera;
        this.semestre = semestre;
        this.materia = materia;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public char getSemestre() {
        return semestre;
    }

    public void setSemestre(char semestre) {
        this.semestre = semestre;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    @Override
    public String toString() {
        return "Alumno{" + "cedula=" + cedula + ", nombreCompleto=" + nombreCompleto + ", carrera=" + carrera + ", semestre=" + semestre + ", materia=" + materia + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.cedula);
        hash = 17 * hash + this.semestre;
        hash = 17 * hash + Objects.hashCode(this.materia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alumno other = (Alumno) obj;
        if (this.semestre != other.semestre) {
            return false;
        }
        if (!Objects.equals(this.cedula, other.cedula)) {
            return false;
        }
        if (!Objects.equals(this.materia, other.materia)) {
            return false;
        }
        return true;
    }

}
