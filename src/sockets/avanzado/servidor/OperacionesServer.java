package sockets.avanzado.servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OperacionesServer {
    private int puertoServidor;

    private Socket miServicio = null;
    private ServerSocket socketServicio = null;
    private DataOutputStream salidaDatos = null;
    private DataInputStream entradaDatos = null;

    private boolean repetirIteracion = true;
    private boolean terminarServidor = false;
    private List<Alumno> listAlumno = new ArrayList<>();

    public int getPuertoServidor() {
        return puertoServidor;
    }

    public OperacionesServer(int puertoServidor) {
        this.puertoServidor = puertoServidor;
        this.conexion();
    }

    private void conexion() {
        try {
            socketServicio = new ServerSocket(this.getPuertoServidor());
            miServicio = socketServicio.accept();
            System.out.printf("***El Servidor esta escuchando el puerto:%d***\n", this.getPuertoServidor());

            Thread subProceso = new Thread(new Runnable() {
                @Override
                public void run() {
                    recibirDatos();
                }
            });

            subProceso.start();

        } catch (BindException e) {
            Logger.getLogger(OperacionesServer.class.getName()).log(Level.WARNING, "El puerto esta en uso - Puerto " + this.getPuertoServidor(), e.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(OperacionesServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void recibirDatos() {
        while (repetirIteracion) {
            try {
                InputStream inputStream = null;
                String lineaRecibida = null;
                String parametroRecibido[];

                inputStream = miServicio.getInputStream();
                entradaDatos = new DataInputStream(inputStream);

                lineaRecibida = entradaDatos.readUTF();
                parametroRecibido = lineaRecibida.split("-");
                this.calcularResultado(parametroRecibido);

            } catch (IOException ex) {
                try {
                    if (miServicio != null) {
                        miServicio.close();
                    }

                    if (entradaDatos != null) {
                        entradaDatos.close();
                    }

                    miServicio = socketServicio.accept();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void calcularResultado(String parametro[]) {
        String escribir = null;

        int opcionRecibida = Integer.valueOf(parametro[0]);
        int cantParam = parametro.length;

        if (validarParametros(opcionRecibida, cantParam)) {
            switch (opcionRecibida) {
                case 1:
                    escribir = agregar(parametro);
                    break;
                case 2:
                    escribir = eliminar(parametro);
                    break;
                case 3:
                    escribir = consultar(parametro);
                    break;
            }
        } else {
            escribir = "Respuesta del Servidor: Cantidad de parametros recibidos incorrectos\n";
        }

        this.enviarDatos(escribir);

    }

    private void enviarDatos(String datos) {
        try {
            OutputStream outputStream = null;
            outputStream = miServicio.getOutputStream();
            salidaDatos = new DataOutputStream(outputStream);
            salidaDatos.writeUTF(datos);
            salidaDatos.flush();

            if (terminarServidor) {
                this.cerrarTodo();
                this.repetirIteracion = false;
            }

        } catch (IOException ex) {
            Logger.getLogger(OperacionesServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cerrarTodo() {
        try {

            if (salidaDatos != null) {
                salidaDatos.close();
            }

            if (entradaDatos != null) {
                entradaDatos.close();
            }

            if (socketServicio != null) {
                socketServicio.close();
                System.out.printf("***El Servidor en el puerto:%d esta abajo***\n", this.getPuertoServidor());
            }

            if (miServicio != null) {
                miServicio.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(OperacionesServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean validarParametros(int opcion, int cantidadParametros) {

        if (opcion == 1 && cantidadParametros == 6) {
            return true;
        }

        if (opcion == 2 && cantidadParametros == 4) {
            return true;
        }

        if (opcion == 3 && cantidadParametros == 2) {
            return true;
        }

        return false;
    }

    private String agregar(String[] parametro) {
        Alumno a = new Alumno(parametro[1], parametro[2], parametro[3], parametro[4].charAt(0), parametro[5]);
        for (Alumno alumno : listAlumno) {
            if (alumno.equals(a))
                return "El alumno ya esta inscripto en la materia y semestre ingresados " + a.toString();
        }
        listAlumno.add(a);
        return "Alumno agregado " + a.toString();
    }

    private String eliminar(String[] parametro) {
        Iterator<Alumno> itAlumno = listAlumno.iterator();
        String cedula = parametro[1];
        String materia = parametro[2];
        char semestre = parametro[3].charAt(0);

        while (itAlumno.hasNext()) {
            Alumno alumActual = itAlumno.next();
            if (alumActual.getCedula().equals(cedula)
                    && alumActual.getMateria().equals(materia)
                    && alumActual.getSemestre() == semestre) {
                String result = alumActual.toString();
                itAlumno.remove();
                return "Alumno eliminado correctamente " + result;
            }
        }

        return "No existe alumno para la materia y semestre ingresado";
    }

    private String consultar(String[] parametro) {
        Iterator<Alumno> itAlumno = listAlumno.iterator();
        String cedula = parametro[1];
        String result = "";

        while (itAlumno.hasNext()) {
            Alumno alumActual = itAlumno.next();
            if (alumActual.getCedula().equals(cedula)) {
                result = result + alumActual.getMateria() + "-" + alumActual.getSemestre() + "-" + alumActual.getCarrera() + "\n";
            }
        }

        if (result.isEmpty()) {
            return "No existe datos para el alumno ingresado";
        }

        return "Listado de materia del alumno: \n" + result;
    }
}
