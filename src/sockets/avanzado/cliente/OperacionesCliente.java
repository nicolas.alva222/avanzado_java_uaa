package sockets.avanzado.cliente;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OperacionesCliente {
	private Socket socketCliente = null;
    private BufferedReader entrada = null;
    private DataInputStream entradaDatos = null;
    private DataOutputStream salidaDatos = null;
    private int numeroPuerto;
    private String ipMaquina;

    public int getNumeroPuerto() {
        return numeroPuerto;
    }

    public String getIpMaquina() {
        return ipMaquina;
    }

    public OperacionesCliente(int numeroPuerto, String ipMaquina) {
        this.numeroPuerto = numeroPuerto;
        this.ipMaquina = ipMaquina;
        this.conexion();
    }

    private void conexion() {
        try {
            socketCliente = new Socket(this.getIpMaquina(), this.getNumeroPuerto());

            Thread subProceso = new Thread(new Runnable() {
                @Override
                public void run() {
                    ingresarDatos();
                }
            });

            subProceso.start();

        } catch (ConnectException con) {
            Logger.getLogger(OperacionesCliente.class.getName()).log(Level.WARNING, "Por favor, verifique que el servidor este levantado en la maquina con IP: " + this.getIpMaquina() + " y escuche el PUERTO: " + this.getNumeroPuerto(), con.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(OperacionesCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void ingresarDatos() {
        boolean repetirIteracion = true, errorIngresarDatos = true;
        while (repetirIteracion) {

            try {
                entrada = new BufferedReader(new InputStreamReader(System.in));
                int opcion = 0;
                while (opcion != 4) {
                    System.out.println("------------------------------------");
                    System.out.println("Opciones disponibles");
                    System.out.println("------------------------------------");
                    System.out.println("1- Agregar alumno a una materia");
                    System.out.println("2- Eliminar alumno de una materia");
                    System.out.println("3- Consultar materias");
                    System.out.println("4- Salir");
                    System.out.println("------------------------------------");
                    System.out.println("Seleccione una opcion:");

                    do {
                        try {
                            opcion = Integer.parseInt(entrada.readLine());
                            errorIngresarDatos = false;
                        } catch (NumberFormatException num) {
                            System.out.println("**Ingrese una opcion de 1 a 4**");
                            errorIngresarDatos = true;
                        }
                    } while (errorIngresarDatos);

                    if (opcion < 1 || opcion > 4) {
                        System.out.println("**Ingrese una opcion de 1 a 4**");
                        continue;
                    }

                    if (opcion == 4) {
                        repetirIteracion = false;
                        continue;
                    }

                    if (opcion >= 1 && opcion <= 3) {
                        switch (opcion) {
                            case 1:
                                agregarAlumno(opcion);
                                break;
                            case 2:
                                eliminarAlumno(opcion);
                                break;
                            case 3:
                                consultarMateria(opcion);
                                break;
                        }
                    }

                    escucharDatos(socketCliente);
                    Thread.sleep(1000);
                }

            } catch (Exception ex) {
                Logger.getLogger(OperacionesCliente.class.getName()).log(Level.SEVERE, null, ex);

            }

        }
        this.cerrarTodo();
    }

    private boolean escucharDatos(Socket socket) throws IOException {
        InputStream inputStream = null;
        String cadenaRecibida = null;
        inputStream = socket.getInputStream();
        entradaDatos = new DataInputStream(inputStream);
        cadenaRecibida = entradaDatos.readUTF();
        System.out.println(cadenaRecibida);

        return false;
    }

    private void enviarDatos(String datos) {

        try {
            OutputStream outputStream = null;
            outputStream = socketCliente.getOutputStream();
            salidaDatos = new DataOutputStream(outputStream);
            salidaDatos.writeUTF(datos);
            salidaDatos.flush();
        } catch (IOException ex) {
            Logger.getLogger(OperacionesCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cerrarTodo() {
        try {

            if (salidaDatos != null) {
                salidaDatos.close();
            }

            if (entradaDatos != null) {
                entradaDatos.close();
            }

            if (socketCliente != null) {
                socketCliente.close();
                System.out.printf("***El Socket en el puerto:%d se bajo***\n", this.getNumeroPuerto());
            }

        } catch (IOException ex) {
            Logger.getLogger(OperacionesCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void agregarAlumno(int opcion) throws IOException {
        String cedula, nombreCompleto, carrera, materi;
        char semestre;
        System.out.println("Ingrese la cedula del alumno");
        cedula = entrada.readLine();

        System.out.println("Ingrese el nombre del alumno");
        nombreCompleto = entrada.readLine();

        System.out.println("Ingrese la carrera del alumno");
        carrera = entrada.readLine();

        System.out.println("Ingrese el semestre del alumno(O,P,V)");
        semestre = entrada.readLine().charAt(0);

        System.out.println("Ingrese la materia del alumno");
        materi = entrada.readLine();
        
        enviarDatos(opcion + "-" + cedula + "-" + nombreCompleto + "-" + carrera + "-" + semestre + "-" + materi);
    }

    private void eliminarAlumno(int opcion) throws IOException {
       String cedula,materi;
        char semestre;
        System.out.println("Ingrese la cedula del alumno");
        cedula = entrada.readLine();

        System.out.println("Ingrese la materia del alumno");
        materi = entrada.readLine();

        System.out.println("Ingrese el semestre del alumno(O,P,V)");
        semestre = entrada.readLine().charAt(0);
       
        enviarDatos(opcion + "-" + cedula + "-" + materi + "-" + semestre);
    }

    private void consultarMateria(int opcion) throws IOException {
        String cedula;
        
        System.out.println("Ingrese la cedula del alumno");
        cedula = entrada.readLine();        
        
        enviarDatos(opcion + "-" + cedula);
    }


}
