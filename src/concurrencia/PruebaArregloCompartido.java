package concurrencia;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PruebaArregloCompartido {

    public static void main(String[] args) {
        ArregloSimple arregloS = new ArregloSimple(12);
        EscritorArreglo escritorA1 = new EscritorArreglo(1, arregloS);
        EscritorArreglo escritorA2 = new EscritorArreglo(100, arregloS);

        ExecutorService ejecutor = Executors.newCachedThreadPool();
        ejecutor.execute(escritorA1);
        ejecutor.execute(escritorA2);
        ejecutor.shutdown();

        try {
            boolean tareaFin = ejecutor.awaitTermination(20, TimeUnit.SECONDS);

            if (tareaFin)
                System.out.println(arregloS.toString());
            else
                System.err.println("Existen tareas activas que no fueron terminadas");

        } catch (InterruptedException ex) {
            System.err.println("Se agoto el tiempo de espera de las tareas");
        }

    }

}
