package concurrencia;

import java.util.Random;

public class ArregloSimple {

    private final int arreglo[]; // el arreglo entero compartido
    private int indiceEscritura = 0; // indice del siguiente elemento a escribir
    private final static Random generador = new Random();
    public ArregloSimple(int tamanio) {
        arreglo = new int[tamanio];
    }

    //
    public synchronized void agregar(int valor) {

        int posicion = indiceEscritura;
        try {
            Thread.sleep(generador.nextInt(3000));
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        arreglo[posicion] = valor;
        System.out.printf("Se agrego -> %2d, indice -> %d, thread -> %s \n",
                valor,
                posicion,
                Thread.currentThread().getName());

        ++indiceEscritura;
        System.out.printf("\t\tSiguiente indice de escritura: %d\n", indiceEscritura);

    }

    public String toString() {

        String cadenaArreglo = "\nContenido de ArregloSimple:\n";
        for (int i = 0; i < arreglo.length; i++) {
            cadenaArreglo += arreglo[i] + " ";
        }
        return cadenaArreglo;

    }

}
