package concurrencia.sinsincronizacion;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PrincipalconExecutor {

    public static void main(String[] args) {
        System.out.println("Creacion de hilos");
        BufferSinSincronizacion datoCompartido = new BufferSinSincronizacion();

        ExecutorService ejecutor = Executors.newCachedThreadPool();

        ejecutor.execute(new Productor(datoCompartido));
        ejecutor.execute(new Thread(new Consumidor(datoCompartido)));

        ejecutor.shutdown();

        System.out.println("Finalizaron los procesos");

    }

}
