package concurrencia.sinsincronizacion;

public class PrincipalconThread {

    public static void main(String[] args) {
        System.out.println("Creacion de hilos");
        BufferSinSincronizacion datoCompartido = new BufferSinSincronizacion();

        Thread productor = new Thread(new Productor(datoCompartido));
        Thread consumidor = new Thread(new Consumidor(datoCompartido));

        productor.start();
        consumidor.start();

        System.out.println("Finalizaron los procesos");


    }

}