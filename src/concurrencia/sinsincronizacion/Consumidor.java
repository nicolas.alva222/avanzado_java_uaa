package concurrencia.sinsincronizacion;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Consumidor implements Runnable {
    private final Buffer datoCompartido;
    private final static Random generador = new Random();

    public Consumidor(Buffer datoCompartido) {
        this.datoCompartido = datoCompartido;
    }

    @Override
    public void run() {
        int suma = 0;
        for (int i = 1; i < 10; i++) {
            try {
                Thread.sleep(generador.nextInt(3000));

                suma = datoCompartido.get();
                System.out.println("El valor de la suma en Consumidor es: " + suma);
            } catch (InterruptedException ex) {
                Logger.getLogger(Consumidor.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.err.println("Consumidor finaliza tarea");
    }
}
