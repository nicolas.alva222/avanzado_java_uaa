package concurrencia.sinsincronizacion;

public class BufferSinSincronizacion implements Buffer {
    private int bufer = -1;

    @Override
    public void set(int valor) {
        System.out.println("Productor escribe el valor : " + valor);
        bufer = valor;
    }

    @Override
    public int get() {
        System.out.println("Consumidor lee el valor :" + bufer);
        return bufer;
    }
}
