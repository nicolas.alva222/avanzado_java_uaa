package concurrencia;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EjecutarTareas {

    public static void main(String[] args) {

        TareaImprimir tarea1 = new TareaImprimir("tarea1");
        TareaImprimir tarea2 = new TareaImprimir("tarea2");
        TareaImprimir tarea3 = new TareaImprimir("tarea3");

        System.out.println("Creacion de objeto ExecutorService");
        ExecutorService executorService = Executors.newCachedThreadPool();

        System.out.println("Subprocesos creados, iniciando...");
        executorService.execute(tarea1);
        executorService.execute(tarea2);
        executorService.execute(tarea3);

        executorService.shutdown();
        System.out.println("Tareas iniciadas, main termina.\n");

    }
}