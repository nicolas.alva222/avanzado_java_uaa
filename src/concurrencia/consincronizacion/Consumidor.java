package concurrencia.consincronizacion;

import java.util.Random;

public class Consumidor implements Runnable {

    private final static Random generador = new Random();
    private final Bufer ubicacionCompartida; // referencia al objeto compartido
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";


    public Consumidor(Bufer compartido) {
        ubicacionCompartida = compartido;
    }

    @Override
    public void run() {
        int suma = 0;
        for (int cuenta = 1; cuenta <= 10; cuenta++) {
            try {
                Thread.sleep(generador.nextInt(3000));
                suma += ubicacionCompartida.get();
                System.out.printf(ANSI_GREEN + "Suma consumidor -> %d\n" + ANSI_RESET, suma);
            } catch (InterruptedException excepcion) {
                excepcion.printStackTrace();
            }
        }
        System.out.printf("\n%s %d\n%s\n", "Consumidor leyo valores, el total es", suma, "**********Terminando Consumidor**********");
    }

}