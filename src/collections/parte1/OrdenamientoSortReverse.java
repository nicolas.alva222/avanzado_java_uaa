package collections.parte1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OrdenamientoSortReverse {

    private static final String palos[] = {"Corazones", "Diamantes", "Bastos", "Espadas"};

    public void imprimirElementos() {
        List<String> lista = Arrays.asList(palos);

        System.out.printf("Elementos del arreglo desordenados:\n%s\n", lista);
        Collections.sort(lista, Collections.reverseOrder());

        System.out.printf("Elementos de lista ordenados:\n%s\n", lista);
    }

    public static void main(String args[]) {
        OrdenamientoSortReverse ordenamiento = new OrdenamientoSortReverse();
        ordenamiento.imprimirElementos();
    }
}