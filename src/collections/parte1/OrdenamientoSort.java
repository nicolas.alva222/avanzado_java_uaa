package collections.parte1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OrdenamientoSort {

    private static final String palos[] = {"CORAZONES", "Diamantes", "Bastos", "Espadas"};

    // muestra los elementos del arreglo
    public void imprimirElementos() {
        List<String> lista = Arrays.asList(palos);
        System.out.printf("Elementos del arreglo desordenados:\n%s\n", lista);

        Collections.sort(lista); // ordena ArrayList

        // imprime lista
        System.out.printf("Elementos del arreglo ordenados:\n%s\n", lista);
    }

    public static void main(String args[]) {
        OrdenamientoSort orden1 = new OrdenamientoSort();
        orden1.imprimirElementos();
    }
}
