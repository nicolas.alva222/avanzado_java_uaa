package collections.parte1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class OperacionesProducto {
    private List<Producto> listaProductos = new ArrayList<Producto>();
    private BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));

    public void Menu() throws IOException {

        int opcion = 0;

        while (opcion != 6) {

            System.out.println("Selecione una opcion");
            System.out.println("1- Agregar Producto");
            System.out.println("2- Actualizar Producto segun codigo");
            System.out.println("3- Eliminar Producto segun codigo");
            System.out.println("4- Listar Producto");
            System.out.println("5- Ordenar Producto en forma asc por precio");
            System.out.println("6- Salir");

            System.out.println("Ingrese una opcion");

            opcion = Integer.parseInt(entrada.readLine());

            switch (opcion) {
                case 1:
                    agregarProducto();
                    break;
                case 2:
                    actualizarProducto();
                    break;
                case 3:
                    eliminarProducto();
                    break;
                case 4:
                    System.out.println("*** Listado de producto ***");
                    imprimirListaProducto();
                    break;
                case 5:
                    ordenar();
                    break;
                case 6:
                    System.out.println("*** Hasta luego ***");
                    break;
                default:
                    System.out.println("*** Opcion incorrecta ***");
            }

        }

    }

    private void agregarProducto() throws IOException {
        Producto productoNuevo = new Producto();

        System.out.println("Ingrese código");
        productoNuevo.setCodigo(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese nombre");
        productoNuevo.setNombre(entrada.readLine());

        System.out.println("Ingrese procedencia");
        productoNuevo.setProcedencia(entrada.readLine());

        System.out.println("Ingrese la cantidad en Stock");
        productoNuevo.setCantidadStock(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese el precio");
        productoNuevo.setPrecio(Integer.parseInt(entrada.readLine()));

        listaProductos.add(productoNuevo);
        System.out.println("*** Producto agregado ***");

    }

    private void imprimirListaProducto() {

        for (Producto productoElemento : listaProductos) {
            System.out.println(productoElemento.toString());
        }
    }

    private void ordenar() {
        System.out.println("*** Listado de productos antes de ordenar ***");
        imprimirListaProducto();
        listaProductos.sort(new Comparator<Producto>() {
            @Override
            public int compare(Producto p1, Producto p2) {
                return new Integer(p1.getPrecio()).compareTo(new Integer(p2.getPrecio()));
            }
        });
        System.out.println("*** Listado de productos después de ordenar ***");
        imprimirListaProducto();
    }

    private void eliminarProducto() throws IOException {
        System.out.println("Ingrese código del producto a eliminar");
        int codigoProducto = Integer.parseInt(entrada.readLine());

        Iterator<Producto> itProducto = listaProductos.iterator();
        while (itProducto.hasNext()) {
            Producto productoActual = itProducto.next();
            if (productoActual.getCodigo() == codigoProducto) {
                itProducto.remove();
                System.out.println("*** Producto eliminado ***");
                return;
            }
        }
        System.out.println("*** Código de producto inexistente ***");
    }

    private void actualizarProducto() throws IOException {
        int indice = 0;
        System.out.println("Ingrese código del producto a actualizar");
        int codigoProducto = Integer.parseInt(entrada.readLine());

        Iterator<Producto> itProducto = listaProductos.iterator();
        while (itProducto.hasNext()) {
            Producto productoActual = itProducto.next();
            if (productoActual.getCodigo() == codigoProducto) {

                System.out.println("Ingrese cantidad de stock");
                int cantidad = Integer.parseInt(entrada.readLine());
                productoActual.setCantidadStock(cantidad);
                listaProductos.set(indice, productoActual);
                return;
            }
            indice++;
        }
        System.out.println("*** Código de producto inexistente ***");

    }
}
