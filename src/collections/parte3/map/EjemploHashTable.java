package collections.parte3.map;

import java.util.Enumeration;
import java.util.Hashtable;

public class EjemploHashTable {

    public static void main(String[] args) {

        Hashtable<String,String> paises = new Hashtable<>();

        if (paises.isEmpty()){
            System.out.println("Mapa vacio");
        }

        paises.put("PY", "PARAGUAY");
        paises.put("UY", "URUGUAY");
        paises.put("ES", "ESPAÑA");
        paises.put("BR", "BRASIL");
        paises.put("USA", "ESTADOS UNIDOS");
        //paises.put("BR", "OTRO PAIS");

        if (!paises.isEmpty()){
            System.out.println("Mapa cargado");
        }

        Enumeration<String> keys = paises.keys();

        String elemento;
        while (keys.hasMoreElements()) {
            elemento = keys.nextElement();
            System.out.println(elemento + " ------> " + paises.get(elemento));
        }

    }

}