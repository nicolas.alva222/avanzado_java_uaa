package collections.parte3.map;

import java.util.HashMap;

public class EjemploHashMap {

    public static void main(String[] args) {

        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put(1, "JOSE");
        mapa.put(2, "JULIA");
        mapa.put(3, "MARIA");
        mapa.put(4, "MARCELO");
        mapa.put(5, "LILIAN");
        mapa.put(6, "JAVIER");
        mapa.put(7, "ROSSANA");
        mapa.put(8, "ANDRES");
        mapa.put(null, "NOMBRE A");

        if (mapa.containsKey(11)){
            System.out.println("Clave encontrada");
        } else {
            System.out.println("Clave inexistente");
        }

        System.out.println("\n\nValores del HashMap\n");
        String[] names = mapa.values().toArray(new String[0]);
        for (String nombre : names) {
            System.out.println(nombre);
        }

        System.out.println("\n\nClaves del HashMap\n");
        Integer[] keys = mapa.keySet().toArray(new Integer[0]);
        for (Integer clave : keys) {
            System.out.println(clave);
        }

    }

}