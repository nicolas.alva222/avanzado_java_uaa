package collections.parte2.set;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Principal {
    public static void main(String[] args) {
        OperacionesPrestamos opp = new OperacionesPrestamos();
        try {
            opp.Menu();
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
