package collections.parte2.queue;

import java.util.PriorityQueue;

public class Principal {
    public static void main(String[] args) {

        PriorityQueue<Empleado> empleados = new PriorityQueue<>();

        empleados.offer(new Empleado("Rafael", 100000.00));
        empleados.offer(new Empleado("Jose", 145000.00));
        empleados.offer(new Empleado("Andrea", 115000.00));
        empleados.offer(new Empleado("Jazmin", 167000.00));

        while (!empleados.isEmpty()) {
            System.out.println(empleados.poll());
        }
    }
}
