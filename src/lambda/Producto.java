package lambda;

public class Producto {

    private String nombre;
    private String categoriaProducto;
    private Double precio;
    private int costo;
    private String proveedor;

    public Producto(String nombre, String categoriaProducto, Double precio, int costo, String proveedor) {
        super();
        this.nombre = nombre;
        this.categoriaProducto = categoriaProducto;
        this.precio = precio;
        this.costo = costo;
        this.proveedor = proveedor;
    }

    public Producto() {
        super();
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoriaProducto() {
        return categoriaProducto;
    }

    public void setCategoriaProducto(String categoriaProducto) {
        this.categoriaProducto = categoriaProducto;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public String toString() {
        return "Producto [nombre=" + nombre
                + ", categoriaProducto=" + categoriaProducto
                + ", precio=" + precio
                + ", costo=" + costo
                + ", proveedor=" + proveedor + "]";
    }


}
