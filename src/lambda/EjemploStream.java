package lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EjemploStream {

    public static void main(String[] args) {

        List<Producto> pro = new ArrayList<Producto>();

        pro.add(new Producto("Leche", "Lacteos", 2500.0, 2300, "Proveedor a"));
        pro.add(new Producto("Manteca", "Lacteos", 2850.0, 2560, "Proveedor b"));
        pro.add(new Producto("Manteca", "Lacteos", 2900.0, 2560, "Proveedor a"));
        pro.add(new Producto("Yogurt", "Lacteos", 3300.0, 3000, "Proveedor d"));
        pro.add(new Producto("Yogurt", "Lacteos", 3200.0, 3000, "Proveedor c"));
        pro.add(new Producto("Costilla", "CarnicOs", 10200.0, 9975, "Proveedor d"));
        pro.add(new Producto("Vacio", "Carnicos", 10300.0, 11500, "Proveedor d"));
        pro.add(new Producto("Rabadilla", "Carnicos", 10400.0, 11000, "Proveedor d"));
        pro.add(new Producto("Chorizo con queso", "Carnicos", 15500.0, 18000, "Proveedor d"));

        /**
         * Imprime los proveedores
         */
        //Stream<Producto> variableStreams1 = pro.stream();
        //variableStreams1.forEach(x -> System.out.println(x.getProveedor()));

        //System.out.println();
        //System.out.println();


        /**
         * Imprime el nombre de los productos
         */
        //Stream<String> variableStreams2 = pro.stream().map(Producto::getNombre);
        //variableStreams2.forEach(nombreProd -> System.out.println(nombreProd));

        //System.out.println();
        //System.out.println();

        /**
         * Filtro por precio
         */
        //Stream<String> variableStreams3 = pro.stream()
        //        .filter(p -> p.getPrecio().doubleValue() > 3000.0)
        //        .map(Producto::toString);

        //variableStreams3.forEach(producto -> System.out.println(producto));

        //System.out.println();
        //System.out.println();


        //pro.stream()
        //        .filter(p -> p.getPrecio().doubleValue() > 3000.0)
        //        .map(Producto::toString)
        //        .forEach(productoFinal -> System.out.println(productoFinal));

        //System.out.println();
        //System.out.println();


        /**
         * Filtro por costo y ordenado en forma ascendente por costo
         */
        //Stream<String> variableStreams4 = pro.stream()
        //        .filter(p -> p.getCosto() > 2500)
        //        .sorted(Comparator.comparingInt(Producto::getCosto))
        //        .map(Producto::toString);

        //variableStreams4.forEach(producto -> System.out.println(producto));

        //System.out.println();
        //System.out.println();

        /**
         * Filtro por costo y ordenado en forma descendente por costo
         */
        //Stream<String> variableStreams5 = pro.stream()
        //        .filter(p -> p.getCosto() > 2500)
        //        .sorted(Comparator.comparingInt(Producto::getCosto).reversed())
        //        .map(Producto::toString);

        //variableStreams5.forEach(producto -> System.out.println(producto));

        //System.out.println();
        //System.out.println();

        /**
         * Filtro por costo y ordenado por dos valores
         */
        //Stream<String> variableStreams6 = pro.stream()
        //        .filter(p -> p.getCosto() > 2500)
        //        .sorted(Comparator.comparing(Producto::getCosto)
        //                .thenComparing(Comparator.comparing(Producto::getProveedor))
        //        )
        //        .map(Producto::toString);

        //variableStreams6.forEach(producto -> System.out.println(producto));

        //System.out.println();
        //System.out.println();

        /**
         * Ordenamiento por mas valores cliente asc y otro desc
         */
        //Stream<String> variableStreams7 = pro.stream()
        //        .filter(p -> p.getCosto() > 2500)
        //        .sorted(Comparator.comparing(Producto::getCosto)//.reversed()
        //                .thenComparing(Collections.reverseOrder(Comparator.comparing(Producto::getProveedor)))
        //        )
        //        .map(Producto::toString);

        //variableStreams7.forEach(producto -> System.out.println(producto));

        //System.out.println();
        //System.out.println();

        /**
         * Operacion de agrupacion
         */
        //Map<String, Long> variableStreams8 = pro.stream()
        //        .collect(Collectors.groupingBy(Producto::getProveedor, Collectors.counting()));

        //variableStreams8.forEach((proveedor, cantidad) -> System.out.printf("Proveedor %s, Cantidad %d \n", proveedor, cantidad));

        //System.out.println();
        //System.out.println();

        /**
         * Operacion de filtrado agrupacion
         */
        //Map<String, Long> variableStreams9 = pro.stream()
        //        .filter(p -> p.getCosto() > 3000)
        //        .collect(Collectors.groupingBy(Producto::getProveedor, Collectors.counting()));

        //variableStreams9.forEach((proveedor, cantidad) -> System.out.printf("Proveedor %s, Cantidad %d \n", proveedor, cantidad));

        //System.out.println();
        //System.out.println();

        /**
         * Agrupar por proveedor y suma de los precios
         */
        //Map<String, Double> variableStreams10 = pro.stream()
        //        .collect(
        //                Collectors.groupingBy(Producto::getProveedor,
        //                        Collectors.summingDouble(
        //                                Producto::getPrecio
        //                        ))
        //        );

        //variableStreams10.forEach((proveedor, precioTotal) -> System.out.printf("Proveedor %s, Precio total %.0f \n", proveedor, precioTotal));

        //System.out.println();
        //System.out.println();


        //
        //pro.stream()
        //        .limit(3)
        //        .map(Producto::toString)
        //        .forEach(productoFinal -> System.out.println(productoFinal));

        //System.out.println();
        //System.out.println();

        //
        //Optional<Producto> maximoPro = pro.stream()
        //        .max(Comparator.comparing(Producto::getCosto));

        //System.out.println(maximoPro.get());

        //System.out.println();
        //System.out.println();

        //
        //Double promedioGeneral = pro.stream().collect(Collectors.averagingInt(Producto::getCosto));
        //System.out.printf("El promedio general es: %.0f", promedioGeneral);

        //System.out.println();
        //System.out.println();

        //
        //Double promedioXCategoria = pro.stream().
        //        filter(p -> p.getCategoriaProducto().equalsIgnoreCase("Carnicos"))
        //        .collect(Collectors.averagingInt(Producto::getCosto));
        //System.out.printf("Promedio de la categoria Carnicos: %.0f", promedioXCategoria);

        //System.out.println();
        //System.out.println();


    }

}
