package generic;

public class App {
    private Integer[] arregloInteger = { 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20  };
    private Double[] arregloDouble = { 1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0  };

    private Pila<Integer> pilaInteger;
    private Pila<Double> pilaDouble;

    public static void main (String args[])
    {
        App pilaExample = new App();
        try {
            pilaExample.pruebaPilas();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pruebaPilas() throws InterruptedException
    {
        pilaDouble = new Pila<Double>(10);
        pruebaPushDouble();
        pruebaPopDouble();

        pilaInteger = new Pila<Integer>(5);
        pruebaPushInteger();
        pruebaPopInteger();
    }

    private void pruebaPushDouble() throws InterruptedException
    {
        try {
            Thread.sleep(3000);
            System.out.println("Metiendo elementos en pilaDouble");
            for (Double elemento : arregloDouble) {
                System.out.printf("%.0f ", elemento.doubleValue());
                pilaDouble.push(elemento);
                Thread.sleep(1000);
            }
            System.out.println();
        } catch (ErrorPilaLlena e) {
            System.out.println();
            System.err.println();
            e.printStackTrace();
        }
    }

    private void pruebaPopDouble() throws InterruptedException
    {
        try {
            Thread.sleep(3000);
            System.out.println("Sacando elementos en pilaDouble");
            double valorSacador;
            while (true) {
                valorSacador = pilaDouble.pop();
                System.out.printf("%.0f ", valorSacador);
                Thread.sleep(1000);
            }

        } catch (ErrorPilaVacia e) {
            System.out.println();
            System.err.println();
            e.printStackTrace();
        }
    }

    private void pruebaPushInteger() throws InterruptedException
    {
        try {
            Thread.sleep(3000);
            System.out.println("Metiendo elementos en pilaInteger");
            for (Integer elemento : arregloInteger) {
                System.out.printf("%d ", elemento.intValue());
                pilaInteger.push(elemento.intValue());
                Thread.sleep(1000);
            }
        } catch (ErrorPilaLlena e) {
            System.out.println();
            System.err.println();
            e.printStackTrace();
        }
    }

    private void pruebaPopInteger() throws InterruptedException
    {
        try {
            Thread.sleep(3000);
            System.out.println("Sacando elementos en pilaInteger");
            Integer valorSacador;
            while (true) {
                valorSacador = pilaInteger.pop().intValue();
                System.out.printf("%d ", valorSacador);
                Thread.sleep(1000);
            }
        } catch (ErrorPilaVacia e) {
            System.out.println();
            System.err.println();
            e.printStackTrace();
        }
    }
}






