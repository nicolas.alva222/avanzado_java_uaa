package generic;

public class ErrorPilaVacia extends RuntimeException
{

    private static final long serialVersionUID = 1L;

    public ErrorPilaVacia() {
        this("La pila esta vacia");
    }

    public ErrorPilaVacia(String exp) {
        super(exp);
    }
}
