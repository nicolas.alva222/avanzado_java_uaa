package generic;

import java.util.ArrayList;
import java.util.Iterator;

public class ClaseGenerica<T> implements Iterable<T> {

    private ArrayList<T> lista = new ArrayList<>();
    private int tope;

    public ClaseGenerica(int tope) {
        super();
        this.tope = tope;
    }

    public void add(T object){
        if (lista.size() <= tope){
            lista.add(object);
        } else {
            throw new RuntimeException("ya no se puede ingresar mas, tope alcanzado");
        }
    }

    @Override
    public Iterator<T> iterator() {
        return lista.iterator();
    }
}
