package generic;

public class AppClaseGenerica {

    public static void main(String[] args) {

        ClaseGenerica<Chocolate> bolsa = new ClaseGenerica<>(3);

        Chocolate c1 = new Chocolate("milka");
        Chocolate c2 = new Chocolate("ferrero");
        Chocolate c3 = new Chocolate("lindt");

        bolsa.add(c1);
        bolsa.add(c2);
        bolsa.add(c3);

        for (Chocolate choco: bolsa)
            System.out.println(choco.getMarca());
    }

}
