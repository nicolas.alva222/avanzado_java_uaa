package generic;

public class ImpresionArreglo {

    public static void main(String[] args) {

        Integer[] arregloInteger = {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
        Double[] arregloDouble = {1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0};
        Character[] arregloCharacter = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};

        //imprimirArreglo(arregloInteger);
        //imprimirArreglo(arregloDouble);
        //imprimirArreglo(arregloCharacter);
        //System.out.printf("%n %n");


        imprimirArregloGenerico(arregloInteger);
        imprimirArregloGenerico(arregloDouble);
        imprimirArregloGenerico(arregloCharacter);
        System.out.printf("%n %n");


        System.out.printf("El valor maximos entre {%s,%s,%s} es %s \n"
                , arregloInteger[0]
                , arregloInteger[2]
                , arregloInteger[5]
                , maximo(arregloInteger[0], arregloInteger[2], arregloInteger[5]));

        System.out.printf("El valor maximos entre {%s,%s,%s} es %s \n"
                , arregloDouble[1]
                , arregloDouble[2]
                , arregloDouble[3]
                , maximo(arregloDouble[1], arregloDouble[2], arregloDouble[3]));

        System.out.printf("El valor maximos entre {%s,%s,%s} es %s \n"
                , arregloCharacter[1]
                , arregloCharacter[3]
                , arregloCharacter[5]
                , maximo(arregloCharacter[1], arregloCharacter[3], arregloCharacter[5]));

        System.out.printf("%n %n");


    }

    public static void imprimirArreglo(Integer[] arregloEntrada) {

        System.out.println("El arreglo contiene los siguiente elementos");
        for (Integer elemento : arregloEntrada) {
            System.out.printf("%d ", elemento);
        }
        System.out.println();
    }

    public static void imprimirArreglo(Double[] arregloEntrada) {

        System.out.println("El arreglo contiene los siguiente elementos");
        for (Double elemento : arregloEntrada) {
            System.out.printf("%.1f ", elemento);
        }
        System.out.println();

    }

    public static void imprimirArreglo(Character[] arregloEntrada) {

        System.out.println("El arreglo contiene los siguiente elementos");
        for (Character elemento : arregloEntrada) {
            System.out.printf("%s ", elemento);
        }
        System.out.println();

    }

    public static <E> void imprimirArregloGenerico(E[] arregloEntrada) {

        System.out.println("El arreglo contiene los siguiente elementos");
        for (E elemento : arregloEntrada) {
            System.out.printf("%s ", elemento);
        }
        System.out.println();
    }

    public static <T extends Comparable<T>> T maximo(T valor1, T valor2, T valor3) {

        T maximo = valor1;

        if (valor2.compareTo(maximo) > 0)
            maximo = valor2;

        if (valor3.compareTo(maximo) > 0)
            maximo = valor3;

        return maximo;
    }

}
