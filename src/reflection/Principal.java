package reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Principal
{

    public static void main(String[] args)
    {
        List<Ordenador> lista = new ArrayList<>();
        Ordenador o1 = new Ordenador("A1", "Ordenador A1", 4);
        Ordenador o2 = new Ordenador("B1", "Ordenador B1", 2);
        lista.add(o1);
        lista.add(o2);

        List<Lavadora> lista2 = new ArrayList<>();
        Lavadora l1 = new Lavadora("L1", "Standard", "Lavadora normal");
        Lavadora l2 = new Lavadora("L2", "Automática", "Lavadora autom�tica");
        lista2.add(l1);
        lista2.add(l2);

        imprimirListaCualquiera(lista);
        imprimirListaCualquiera(lista2);

    }

    public static void imprimirListaCualquiera(List<?> lista)
    {
        for (Object c : lista)
        {
            Method[] metodos = c.getClass().getMethods();
            for (Method m : metodos)
            {
                if (m.getName().equals("getId") || m.getName().equals("getDescripcion"))
                {
                    try
                    {
                        String cadena = (String) m.invoke(c, null);
                        System.out.println(cadena);

                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
                    {
                        Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}
